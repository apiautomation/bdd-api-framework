## Cucumber BDD with RestAssured api automation test framework
Author: Vamsi Krishna V S

### Framework highlights
Near script-less basic API automation test framework. Predefined steps later mentioned in the document allows users to write the api automation tests with limited to no java coding experience. Currently this framework has limited feature where allowing users to the test any combination of response key pair values and validation of json array with elements containing expected string.

#### Software Pre-requsites
- Java 1.8 (JDK set in JAVA_HOME)
- Apache Maven 3.3.9 (Maven set in PATH)
- Any java supported IDE (Eclipse, IntliJ, VSCode )
- GIT (for linux or mac ) / GitBash ( windows)
- Cucumber plugin for Eclipse 
- Lastly internet connection

#### Framework Structure  
``` 
  src/main/java
   |__ StepDefinitionsImpl.java   # class having all the implementation of defined step definitions
   |__ StepDefinitions.java		  # core step definitions class glued to the cucumber runner class
   
   src/test/java
    |__CukeRunner.java                # cucumber runner class to execute the test as junit test or mvn test
    |__features
         |__ tmsandboxapitest.feature # cucumber feature files for writing tests in Gherkin syntax
```

#### Steps to execute 
 1. Clone the bibucket repo
 2. Go to the folder in the terminal/cmd
 3. Tests can be executed three ways  
    a. Run command `mvn test`  
    b. Run `CukeRunner.java` tests as Junit test  
    c. Run `tmsandboxapitest.feature` as the cucumber feature
 
#### Snapshot of the execution report
![ScreenShot](./docs/results-snapshot.png)

### Pre-defined steps
GET call with any path  
`user make GET call for path (.*)`

Validate any statuscode  
`the status code is (\\d+)`

Response validation  
`the response (.*) equal (.*)`

Response validation for json array containing expected text  
`the response having (.*) elements have (.*) equals (.*) and (.*) contains text (.*)`

_Example:_

```cucumber
  Scenario: Validate status code for the Get call
  When user make GET call for path /6327/Details.json?catalogue=false
  Then the status code is 200  
``` 
   


