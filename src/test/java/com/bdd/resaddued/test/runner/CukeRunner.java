package com.bdd.resaddued.test.runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		plugin = {"pretty"},
		glue = {"com.bdd.restassured.stepdefinitions"},
		features = {"src/test/features"})
public class CukeRunner {

}
