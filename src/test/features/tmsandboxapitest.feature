Feature: Test tms sandbox api
  User tests the tms sandbaox api
  
  Background: Set baseuri for testing
		Given the application has the base uri https://api.tmsandbox.co.nz/v1/Categories

  Scenario: Validate status code for the Get call  
  When user make GET call for path /6327/Details.json?catalogue=false
  Then the status code is 200
  
  Scenario Outline: Validate response values for the Get call
    When user make GET call for path /6327/Details.json?catalogue=false
    Then the response <key> equal <value>
    
   Examples:
   		| key        | value         |
      | Name      | Carbon credits |
      | CanRelist | true           |

  Scenario: Validate response json array for the Get call
    When user make GET call for path /6327/Details.json?catalogue=false
    Then the response having Promotions elements have Name equals Gallery and Description contains text 2x larger image
