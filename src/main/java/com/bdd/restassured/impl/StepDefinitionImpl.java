package com.bdd.restassured.impl;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;


import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.path.json.JsonPath;

public class StepDefinitionImpl {
	
	private static Response response;
	private static ValidatableResponse jsonresponse;
	public static  JsonPath jp = null;
		
	 public static void setBaseURI (String uri){
		 
	        RestAssured.baseURI = uri;
	    }
	 
	 public static void invokeAPI (String path) {
		 
		 RestAssured.given().contentType("JSON");
		 response = RestAssured.get(path);
	 }

	public static void verifyStatusCode(int statuscode){
		
		jsonresponse=response.then().statusCode(statuscode);
	}


	public static void response_contains_in_any_order(Map<String,String> responseFields){
		
		for (Map.Entry<String, String> field : responseFields.entrySet()) {
			if(StringUtils.isNumeric(field.getValue())){
				jsonresponse.body(field.getKey(), containsInAnyOrder(Integer.parseInt(field.getValue())));
			}
			else{
				jsonresponse.body(field.getKey(), containsInAnyOrder(field.getValue()));
			}
		}
	}

	public static void responseEquals(String key, String value) {
		
		jp = new JsonPath(response.asString());
		if(StringUtils.isNumeric(value)){
			assertEquals(jp.getInt(key), Integer.parseInt(value));
		}
		else{
			assertEquals(jp.getString(key), value);
		}
	}

	public static void responseWithArrayEquals(String arrayelement,String childelement0,String value,String childelement1, String text) {
		
		jp = new JsonPath(response.asString());
		
		String namejspath=arrayelement+"."+childelement0;
		String descjspath=arrayelement+"."+childelement1;
		
		List<String> namearray =jp.getList(namejspath);
		List<String> descarray =jp.getList(descjspath);

		for (String string : namearray) {
			if (string.equalsIgnoreCase(value)) {
				int index=namearray.indexOf(value);
				assertTrue(childelement1+" contains "+text, descarray.get(index).contains(text));
			}
		}

	}

}
