package com.bdd.restassured.stepdefinitions;

import com.bdd.restassured.impl.StepDefinitionImpl;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefinitions {
	
	
	@Given("the application has the base uri (.*)")
	public void a_book_exists_with_isbn(String uri){
		StepDefinitionImpl.setBaseURI(uri);
	}
	
	@When("user make GET call for path (.*)")
	public void invokeAPIWithPathAndVerb(String path){
		StepDefinitionImpl.invokeAPI( path);
	}

	@Then("the status code is (\\d+)")
	public void validateStatusCode(int statuscode){
		StepDefinitionImpl.verifyStatusCode(statuscode);
	}
	
	@Then("the response (.*) equal (.*)")
	public void verifyresponse(String key, String value) {
		StepDefinitionImpl.responseEquals(key,value);
	}

	@Then("the response having (.*) elements have (.*) equals (.*) and (.*) contains text (.*)")
	public void verifyarrayelemnts(String arrayelement, String childelement0, String value, String childelement1, String text) {
		StepDefinitionImpl.responseWithArrayEquals(arrayelement,childelement0,value,childelement1,text);
	}
}
